import React from "react";
import {
  getLoginUsername,
  getLoginAuthenticated
} from "../../redux/selectors/login";
//import { withRouter } from 'react-router-dom';
//import { bindActionCreators } from 'redux';
import { connect } from "react-redux";

const HomePage = props => {
  const { username, authenticated } = props;
  if (authenticated) {
    return <div>Welcome, {username}</div>;
  } else {
    return <div>Hi, you are not logged in yet.</div>;
  }
};

function mapStateToProps(state) {
  // console.log('state', state);
  return {
    authenticated: getLoginAuthenticated(state),
    username: getLoginUsername(state)
  };
}

const HomePageContainer = connect(
  mapStateToProps,
  null
)(HomePage);
//const WelcomePageWithRouter = withRouter(WelcomePageContainer);

export default HomePageContainer;
