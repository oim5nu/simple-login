import React from "react";
import PropTypes from "prop-types";
import { Message } from "semantic-ui-react";
import { isEmpty } from "../../utils/common";

const FormMessage = ({ errors, errorMessage }) => {
  const hasError = Object.values(errors).some(error => !isEmpty(error));
  const nonEmptyErrors = Object.values(errors).filter(error => !isEmpty(error));
  return (
    <div>
      {hasError && (
        <Message error>
          <Message.Header>There are some errors with inputs</Message.Header>
          <Message.List>
            {nonEmptyErrors.map((error, index) => (
              <Message.Item content={error} key={index} />
            ))}
          </Message.List>
        </Message>
      )}{" "}
      {errorMessage && (
        <Message error>
          <Message.Header>There are some errors returned</Message.Header>
          <Message.List>
            {errorMessage && (
              <Message.Item content={errorMessage} key="errorMessage" />
            )}
          </Message.List>
        </Message>
      )}
    </div>
  );
};

FormMessage.propTypes = {
  errors: PropTypes.object,
  errorMessage: PropTypes.string
};

export default FormMessage;
