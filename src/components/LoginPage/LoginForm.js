import React from "react";
import PropTypes from "prop-types";
//import { NavLink } from 'react-router-dom';
import { Segment, Form, Grid, Button } from "semantic-ui-react";
import FormMessage from "../FormMessage/FormMesage";

const LoginForm = ({
  values,
  errors,
  onChange,
  onSubmit,
  canSubmit,
  isSubmitting,
  errorMessage
}) => (
  <Grid
    verticalAlign="middle"
    centered
    columns={1}
    textAlign="left"
    relaxed
    stretched
    style={{ flexGrow: 1, paddingTop: "5em" }}
    padded
  >
    <Grid.Row>
      <Grid.Column tablet={10} mobile={16} computer={6}>
        <Segment>
          <Form action="/" onSubmit={onSubmit}>
            <Form.Input
              name="username"
              placeholder="User Name"
              label="Username"
              onChange={onChange}
              value={values.username}
              error={!!errors.username}
              required
            />
            <Form.Input
              autoComplete="current-password"
              name="password"
              type="password"
              placeholder="Password"
              label="Password"
              onChange={onChange}
              value={values.password}
              error={!!errors.password}
              required
            />

            <div style={{ textAlign: "left" }}>
              <Button
                type="submit"
                disabled={!canSubmit || !!isSubmitting}
                content={isSubmitting ? "Submitting" : "Login"}
                icon="sign in"
              />
              {/*<NavLink to={'/forgot-password'}> Forgot Password? </NavLink>*/}
            </div>
          </Form>
          <FormMessage errors={errors} errorMessage={errorMessage} />
        </Segment>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

LoginForm.propTypes = {
  values: PropTypes.shape({
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }),
  errors: PropTypes.object,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  canSubmit: PropTypes.bool,
  isSubmitting: PropTypes.bool,
  errorMessage: PropTypes.string
};

export default LoginForm;
