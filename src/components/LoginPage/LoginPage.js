import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter, Redirect } from "react-router-dom";
import update from "immutability-helper";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { loginAction } from "../../redux/actions/login";
import { isEmpty } from "../../utils/common";
import _ from "lodash";
import LoginForm from "./LoginForm";
import {
  getLoginError,
  getLoginUsername,
  getLoginAuthenticated,
  getLoginToken,
  getLoginRedirectToReferrer,
  getLoginSubmitting
} from "../../redux/selectors/login";

const validate = values => {
  const errors = {};
  if (!values.username) {
    errors.username = "Username is required";
  } else if (values.username.length > 15) {
    errors.username = "Must be 15 characters or less";
  }

  if (!values.password) {
    errors.password = "Password is required";
  }
  return errors;
};

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      touched: {},
      submitting: null,
      errorMessage: null,
      canSubmit: false,
      values: {
        username: "",
        password: ""
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkCanSubmit = this.checkCanSubmit.bind(this);
  }

  handleChange(event) {
    const field = event.target.name;
    const value = event.target.value;

    let newState = update(this.state, {
      values: {
        [field]: { $set: value }
      },
      touched: { [field]: { $set: true } },
      errorMessage: { $set: null }
    });

    const errors = validate(newState.values);
    const canSubmit = this.checkCanSubmit(errors);

    const fieldsTouched = Object.keys(_.pickBy(newState.touched, _.Boolean));
    const visibleErrors = _.pick(errors, fieldsTouched);
    const finalState = Object.assign(
      {},
      newState,
      { errors: { ...visibleErrors } },
      { canSubmit }
    );

    this.setState(finalState);
  }

  checkCanSubmit(errors) {
    const values = Object.values(errors);
    return values.every(value => isEmpty(value));
  }

  handleSubmit(event) {
    event.preventDefault();
    const errors = validate(this.state.values);
    if (this.checkCanSubmit(errors) === false) {
      return;
    }
    const { username, password } = this.state.values;

    this.setState({ errors: {}, errorMessage: null, submitting: true }, () => {
      this.props.actions.loginAction(username, password);
    });
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    const { redirectToReferrer, errorMessage, submitting } = this.props;

    if (redirectToReferrer === true) {
      return <Redirect to={from} />;
    }

    return (
      <LoginForm
        values={this.state.values}
        errors={this.state.errors}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
        canSubmit={this.state.canSubmit}
        isSubmitting={submitting}
        errorMessage={errorMessage}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: getLoginError(state),
    authenticated: getLoginAuthenticated(state),
    username: getLoginUsername(state),
    token: getLoginToken(state),
    redirectToReferrer: getLoginRedirectToReferrer(state),
    submitting: getLoginSubmitting(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        loginAction
      },
      dispatch
    )
  };
}

LoginPage.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  actions: PropTypes.shape({
    loginAction: PropTypes.func.isRequired
  })
};

const LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
const LoginPageWithRouter = withRouter(LoginPageContainer);

export default LoginPageWithRouter;
