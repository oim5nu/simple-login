import React, { Component } from "react";
import Aux from "../../hoc/Auxiliary";
import { Content } from "./Layout.scss";
// import Toolbar from '../Navigation/Toolbar/Toolbar';
// import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class Layout extends Component {
  render() {
    return (
      <Aux>
        <main className={Content}>{this.props.children}</main>
      </Aux>
    );
  }
}
export default Layout;
