import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import PrivateRoute from "./hoc/PrivateRoute";
import Layout from "./components/Layout/Layout";
import LoginPage from "./components/LoginPage/LoginPage";
import HomePage from "./components/HomePage/HomePage";

import { connect } from "react-redux";
import {
  getLoginAuthenticated,
  getLoginUsername
} from "./redux/selectors/login";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <PrivateRoute
            exact
            authenticated={this.props.authenticated}
            path="/"
            component={HomePage}
          />
        </Switch>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: getLoginAuthenticated(state),
    user: getLoginUsername(state)
  };
}

export default connect(
  mapStateToProps,
  null
)(App);
