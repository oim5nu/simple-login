import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import reducers from "./redux/reducers/index";
import { default as middlewares, sagaMiddleware } from "./middleware";
import rootSaga from "./redux/sagas/index";

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(...middlewares))
);

sagaMiddleware.run(rootSaga);

export default store;
