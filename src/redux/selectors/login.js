export const getLoginUsername = state => state.login.username;
export const getLoginError = state => state.login.error;
export const getLoginMessage = state => state.login.message;
export const getLoginToken = state => state.login.token;
export const getLoginPassword = state => state.login.password;
export const getLoginAuthenticated = state => state.login.authenticated;
export const getLoginRedirectToReferrer = state =>
  state.login.redirectToReferrer;
export const getLoginSubmitting = state => state.login.submitting;
