import { call, put, select } from "redux-saga/effects";
import { getLoginUsername, getLoginPassword } from "../selectors/login";
import { loginSuccessAction, loginFailureAction } from "../actions/login";

import UserApi from "../../api/userMock";
const findUser = UserApi.findUser;

function* workerLoginSaga() {
  try {
    const user = yield select(getLoginUsername);
    const password = yield select(getLoginPassword);

    if (!user || !password) throw new Error(`Empty username or password Input`);

    const response = yield call(findUser, user, password);

    console.log("response", response);
    const { username, token, message } = response;
    yield put(loginSuccessAction(username, token, message));
  } catch (error) {
    console.log("workerLoginSaga", error);
    yield put(loginFailureAction(error));
  }
}

export { workerLoginSaga };
