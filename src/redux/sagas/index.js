import {
  takeLatest,
  //takeEvery,
  all
} from "redux-saga/effects";
import { LOGIN_REQUEST } from "../actions/actionTypes";
import { workerLoginSaga } from "./login";

function* watchAll() {
  yield all([takeLatest(LOGIN_REQUEST, workerLoginSaga)]);
}

export default watchAll;
