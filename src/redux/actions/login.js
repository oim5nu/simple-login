import {
  LOGIN_REQUEST,
  LOGIN_REQUEST_SUCCESS,
  LOGIN_REQUEST_FAILURE
} from "./actionTypes";

export function loginAction(username, password) {
  return {
    type: LOGIN_REQUEST,
    payload: { username, password }
  };
}

export function loginSuccessAction(username, token, message) {
  return {
    type: LOGIN_REQUEST_SUCCESS,
    payload: { username, token, message }
  };
}

export function loginFailureAction(error) {
  return {
    type: LOGIN_REQUEST_FAILURE,
    payload: { error }
  };
}
