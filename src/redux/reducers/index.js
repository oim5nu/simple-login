import { reducer as formReducer } from "redux-form";
import { combineReducers } from "redux";
import { login } from "./login";

const reducers = combineReducers({
  login,
  form: formReducer
});

export default reducers;
