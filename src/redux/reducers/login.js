import produce from "immer";
import {
  LOGIN_REQUEST,
  LOGIN_REQUEST_SUCCESS,
  LOGIN_REQUEST_FAILURE
} from "../actions/actionTypes";

const initialState = {
  username: "",
  password: "",
  submitting: false,
  authenticated: false,
  error: null,
  message: null,
  token: null,
  redirectToReferrer: false
};

export const login = (state = initialState, action = {}) => {
  return produce(state, draft => {
    switch (action.type) {
      case LOGIN_REQUEST:
        draft.submitting = true;
        draft.error = null;
        draft.message = null;
        draft.authenticated = false;
        draft.username = action.payload.username;
        draft.password = action.payload.password;
        draft.token = null;
        draft.redirectToReferrer = false;
        break;
      case LOGIN_REQUEST_SUCCESS:
        draft.username = action.payload.username;
        draft.password = "";
        draft.authenticated = true;
        draft.token = action.payload.token;
        draft.message = action.payload.message;
        draft.error = null;
        draft.submitting = false;
        draft.redirectToReferrer = true;
        break;
      case LOGIN_REQUEST_FAILURE:
        draft.submitting = false;
        draft.authenticated = false;
        draft.error = action.payload.error;
        draft.message = "";
        draft.token = "";
        draft.redirectToReferrer = false;
        break;
      default:
    }
  });
};
