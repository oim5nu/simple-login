import { delay } from "./common";

export const users = [
  {
    username: "yuan",
    password: "yuan"
  },
  {
    username: "leon",
    password: "leon"
  }
];

class UserApi {
  static findUser(username, password) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const index = users.findIndex(
          user => user.username === username && user.password === password
        );
        if (index === -1) reject(`Invalide username or password`);
        else
          resolve({
            username,
            token: "1234567789",
            message: "Successful login"
          });
      }, delay);
    });
  }
}

export default UserApi;
