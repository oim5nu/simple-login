# Install Instructions

## Requirements: node.js version >=8 and git installed under windows/linux/mac

## Steps
1. Under terminal, run command line, git clone https://oim5nu@bitbucket.org/oim5nu/simple-login.git
2. cd simple-login
3. npm install
4. npm start

Thank you.